package pjatk.ssledziona.pamoandroid;

import android.support.v7.app.AppCompatActivity;
import android.app.ProgressDialog;
import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog loadingDialog;

    public String getIdentity() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public void showLoading() {
        if (null == loadingDialog) {
            loadingDialog = new ProgressDialog(this);
            loadingDialog.setCancelable(false);
            loadingDialog.setMessage("Proszę czekać...");
        }
        loadingDialog.show();
    }

    public void hideLoading() {
        if (null != loadingDialog && loadingDialog.isShowing()) {
            loadingDialog.dismiss();
        }
    }
}
