package pjatk.ssledziona.pamoandroid.fragments;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class NotesFragment extends AbstractFragment {

    private final int queryLimit = 100;

    public NotesFragment() {}

    public Query getQuery(DatabaseReference databaseReference) {
        return databaseReference.child("user-notes")
                .child(getUID()).orderByChild("done").equalTo(false).limitToFirst(queryLimit);
    }
}
