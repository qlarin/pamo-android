package pjatk.ssledziona.pamoandroid;

import android.content.Intent;
import android.os.Bundle;

import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;

import java.util.Map;
import java.util.HashMap;

import pjatk.ssledziona.pamoandroid.fragments.DoneNotesFragment;
import pjatk.ssledziona.pamoandroid.fragments.NotesFragment;

public class StartupActivity extends MainActivity {

    private FragmentPagerAdapter fragmentPagerAdapter;
    private ViewPager viewPager;

    private final Map<String, Class> targetActivities = new HashMap<String, Class>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        fillAccessibleActivities();
        fragmentPagerAdapter = initPagerAdapter();
        viewPager = initViewPager(fragmentPagerAdapter);
        createTabLayout(viewPager);
        launchActivities();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.options_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (R.id.logout_action == menuItem.getItemId()) {
            FirebaseAuth.getInstance().signOut();
            startActivity(new Intent(
                    this,
                    targetActivities.get(getString(R.string.logout_button_text))
            ));
            finish();
            return true;
        } else {
            return super.onOptionsItemSelected(menuItem);
        }
    }

    private void fillAccessibleActivities() {
        targetActivities.put(getString(R.string.logout_button_text), LoginActivity.class);
        targetActivities.put(getString(R.string.notes_create), CreateNoteActivity.class);
    }

    private FragmentPagerAdapter initPagerAdapter() {
        return new FragmentPagerAdapter(getSupportFragmentManager()) {
            private final Fragment[] fragments = new Fragment[] {
                    new NotesFragment(),
                    new DoneNotesFragment()
            };
            private final String[] fragmentNames = new String[] {
                    getString(R.string.notes),
                    getString(R.string.done_notes)
            };
            @Override
            public Fragment getItem(int position) {
                return fragments[position];
            }

            @Override
            public int getCount() {
                return fragments.length;
            }

            @Override
            public CharSequence getPageTitle(int position) {
                return fragmentNames[position];
            }
        };
    }

    private ViewPager initViewPager(FragmentPagerAdapter adapter) {
        ViewPager pager = (ViewPager) findViewById(R.id.pager_startup);
        pager.setAdapter(adapter);
        return pager;
    }

    private void createTabLayout(ViewPager pager) {
        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs_startup);
        tabLayout.setupWithViewPager(pager);
    }

    private void launchActivities() {
        findViewById(R.id.notes_create).setOnClickListener(createListener(getString(R.string.notes_create)));
    }

    private View.OnClickListener createListener(final String target) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(
                        StartupActivity.this,
                        targetActivities.get(target)
                ));
            }
        };
    }
}
