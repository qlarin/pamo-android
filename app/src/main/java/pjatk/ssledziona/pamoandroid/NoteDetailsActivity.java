package pjatk.ssledziona.pamoandroid;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Switch;

import com.google.android.gms.common.api.BooleanResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import pjatk.ssledziona.pamoandroid.models.Note;

public class NoteDetailsActivity extends MainActivity {

    private DatabaseReference databaseReference;
    private DatabaseReference databaseUpdateReference;
    private ValueEventListener valueEventListener;
    private String noteKey;
    private String userId;

    private TextView titleView;
    private TextView descriptionView;
    private TextView tagView;
    private Switch doneSwitch;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_note_details);
        userId = getIdentity();
        noteKey = getIntent().getStringExtra("notes_key");
        if (noteKey == null) {
            throw new IllegalArgumentException("Brak klucza");
        }

        databaseUpdateReference = FirebaseDatabase.getInstance().getReference();
        databaseReference = databaseUpdateReference.child("notes").child(noteKey);

        titleView = (TextView) findViewById(R.id.note_title);
        descriptionView = (TextView) findViewById(R.id.note_description);
        tagView = (TextView) findViewById(R.id.note_tag);
        doneSwitch = (Switch) findViewById(R.id.note_done);
    }

    @Override
    protected void onStart() {
        super.onStart();

        ValueEventListener noteListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Note note = dataSnapshot.getValue(Note.class);
                titleView.setText(note.title);
                descriptionView.setText(note.description);
                tagView.setText(note.tag);
                doneSwitch.setChecked(note.done);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(
                        NoteDetailsActivity.this,
                        "Problem z załadowaniem notatki",
                        Toast.LENGTH_SHORT
                ).show();
            }
        };
        CompoundButton.OnCheckedChangeListener checkedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                updateNote(isChecked);
            }
        };

        databaseReference.addValueEventListener(noteListener);
        valueEventListener = noteListener;
        doneSwitch.setOnCheckedChangeListener(checkedChangeListener);
    }

    @Override
    public void onStop() {
        super.onStop();

        if (valueEventListener != null) {
            databaseReference.removeEventListener(valueEventListener);
        }
    }

    private void updateNote(final Boolean isChecked) {
        databaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Note note = dataSnapshot.getValue(Note.class);
                note.done = isChecked;
                Map<String, Object> noteValues = note.toMap();
                Map<String, Object> updates = new HashMap<>();
                updates.put("/notes/" + noteKey, noteValues);
                updates.put("/user-notes/" + userId + "/" + noteKey, noteValues);
                databaseUpdateReference.updateChildren(updates);;
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {}
        });
    }
}
