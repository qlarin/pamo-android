package pjatk.ssledziona.pamoandroid.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;

import pjatk.ssledziona.pamoandroid.NoteDetailsActivity;
import pjatk.ssledziona.pamoandroid.R;
import pjatk.ssledziona.pamoandroid.models.Note;
import pjatk.ssledziona.pamoandroid.views.NoteViewHolder;

public abstract class AbstractFragment extends Fragment {

    private DatabaseReference databaseReference;
    private FirebaseRecyclerAdapter<Note, NoteViewHolder> firebaseRecyclerAdapter;
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;

    public AbstractFragment() {}

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View rootView = inflater.inflate(R.layout.fragment_notes, container, false);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        recyclerView = (RecyclerView) rootView.findViewById(R.id.notes_list);
        recyclerView.setHasFixedSize(true);
        return rootView;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (firebaseRecyclerAdapter != null) {
            firebaseRecyclerAdapter.cleanup();
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setReverseLayout(true);
        linearLayoutManager.setStackFromEnd(true);
        recyclerView.setLayoutManager(linearLayoutManager);

        Query notesQuery = getQuery(databaseReference);
        firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Note, NoteViewHolder>(
                Note.class,
                R.layout.note_row,
                NoteViewHolder.class,
                notesQuery
        ) {
            @Override
            protected void populateViewHolder(
                    final NoteViewHolder viewHolder,
                    final Note model,
                    final int position
            ) {
                final DatabaseReference notesReference = getRef(position);
                final String notesKey = notesReference.getKey();
                viewHolder.itemView.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(getActivity(), NoteDetailsActivity.class);
                        intent.putExtra("notes_key", notesKey);
                        startActivity(intent);
                    }
                });
                viewHolder.bindToNote(model);
            }
        };
        recyclerView.setAdapter(firebaseRecyclerAdapter);
    }

    public String getUID() {
        return FirebaseAuth.getInstance().getCurrentUser().getUid();
    }

    public abstract Query getQuery(DatabaseReference databaseReference);
}
