package pjatk.ssledziona.pamoandroid.views;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import pjatk.ssledziona.pamoandroid.R;
import pjatk.ssledziona.pamoandroid.models.Note;

public class NoteViewHolder extends RecyclerView.ViewHolder {

    public TextView titleView;
    public TextView tagView;

    public NoteViewHolder (View view) {
        super(view);

        titleView = (TextView) itemView.findViewById(R.id.note_title);
        tagView = (TextView) itemView.findViewById(R.id.note_tag);
    }

    public void bindToNote(Note note) {
        titleView.setText(note.title);
        tagView.setText(note.tag);
    }
}
