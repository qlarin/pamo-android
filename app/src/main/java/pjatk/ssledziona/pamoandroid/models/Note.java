package pjatk.ssledziona.pamoandroid.models;

import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Note {

    public String uid;
    public String username;
    public String title;
    public String description;
    public String tag;
    public boolean done = false;

    public Note() {}

    public Note(String uid, String username, String title, String description, String tag) {
        this.uid = uid;
        this.username = username;
        this.title = title;
        this.description = description;
        this.tag = tag;
    }

    public Map<String,Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("uid", uid);
        result.put("username", username);
        result.put("title", title);
        result.put("description", description);
        result.put("tag", tag);
        result.put("done", done);
        return result;
    }
}
