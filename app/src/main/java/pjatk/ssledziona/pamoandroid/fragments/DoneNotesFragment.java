package pjatk.ssledziona.pamoandroid.fragments;


import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Query;

public class DoneNotesFragment extends AbstractFragment {

    private final int queryLimit = 100;

    public DoneNotesFragment() {}

    public Query getQuery(DatabaseReference databaseReference) {
        return databaseReference.child("user-notes")
                .child(getUID()).orderByChild("done").equalTo(true).limitToFirst(queryLimit);
    }
}
