package pjatk.ssledziona.pamoandroid.listeners;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Toast;

public class CustomOnItemSelectedListener implements AdapterView.OnItemSelectedListener {

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Toast.makeText(parent.getContext(),
            "Wybrano: " + parent.getItemAtPosition(position).toString(),
            Toast.LENGTH_SHORT
        ).show();
    }

    public void onNothingSelected(AdapterView<?> arg) {

    }
}
