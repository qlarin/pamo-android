package pjatk.ssledziona.pamoandroid;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.HashMap;
import java.util.Map;

import pjatk.ssledziona.pamoandroid.listeners.CustomOnItemSelectedListener;
import pjatk.ssledziona.pamoandroid.models.Note;
import pjatk.ssledziona.pamoandroid.models.User;

public class CreateNoteActivity extends MainActivity {

    private DatabaseReference databaseReference;
    private EditText titleField;
    private EditText descriptionField;
    private Spinner tagField;
    private FloatingActionButton submitButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_note);
        databaseReference = FirebaseDatabase.getInstance().getReference();
        initViewFields();
        addListeners();
    }

    private void initViewFields() {
        titleField = (EditText) findViewById(R.id.note_title_field);
        descriptionField = (EditText) findViewById(R.id.note_description_field);
        tagField = (Spinner) findViewById(R.id.note_tag_field);
        submitButton = (FloatingActionButton) findViewById(R.id.note_submit_button);
    }

    private void addListeners() {
        addListenerOnSpinnerItemSelection();
        addListenerOnSubmit();
    }

    private void addListenerOnSubmit() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                submitNote();
            }
        });
    }

    private void addListenerOnSpinnerItemSelection() {
        tagField.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    private void submitNote() {
        final String title = titleField.getText().toString();
        final String description = descriptionField.getText().toString();
        final String tag = tagField.getSelectedItem().toString();

        if (TextUtils.isEmpty(title)) {
            titleField.setError(getString(R.string.required_field_label));
            return;
        }

        if (TextUtils.isEmpty(description)) {
            descriptionField.setError(getString(R.string.required_field_label));
            return;
        }

        lockEditing(true);
        Toast.makeText(this, R.string.creating_label, Toast.LENGTH_SHORT).show();

        final String userId = getIdentity();
        databaseReference.child("users").child(userId).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if (user == null) {
                    Toast.makeText(
                            CreateNoteActivity.this,
                            R.string.user_problem_label,
                            Toast.LENGTH_SHORT
                    ).show();
                } else {
                    createNote(userId, user.getUsername(), title, description, tag);
                }
                lockEditing(false);
                finish();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                lockEditing(false);
            }
        });
    }

    private void lockEditing(boolean disabled) {
        titleField.setEnabled(!disabled);
        descriptionField.setEnabled(!disabled);
        tagField.setEnabled(!disabled);
        if (disabled) {
            submitButton.setVisibility(View.GONE);
        } else {
            submitButton.setVisibility(View.VISIBLE);
        }
    }

    private void createNote(String userId, String username, String title, String description, String tag) {
        String key = databaseReference.child("notes").push().getKey();
        Note note = new Note(userId, username, title, description, tag);
        Map<String, Object> noteValues = note.toMap();
        Map<String, Object> updates = new HashMap<>();
        updates.put("/notes/" + key, noteValues);
        updates.put("/user-notes/" + userId + "/" + key, noteValues);
        databaseReference.updateChildren(updates);
    }
}
